import React from "react";
import "./JobDetail.css";
import { connect } from "react-redux";

const JobDetail = (props) => {
  if (!props.selectedJob) {
    return (
      <div className="jobDetailCard">
        <h2> Select Job to shows Details</h2>
      </div>
    );
  }
  return (
    <div className="jobDetailCard">
      <div className="jobDetailsContent">
        <p
          style={{
            fontWeight: "5000",
            fontSize: "20px",
            paddingTop: "10px",
          }}
        >
          {props.selectedJob.name}
        </p>
        <p
          style={{ fontSize: "15px", paddingTop: "10px", marginBottom: "10px" }}
        >
          {props.selectedJob.location.city},{props.selectedJob.location.country}
        </p>
        <img
          src={props.selectedJob.logo}
          alt="logo"
          style={{ width: "80%", alignSelf: "center" }}
        />

        <p
          style={{
            marginTop: "5px",
            marginBottom: "5px",
            fontSize: "15px",
            paddingLeft: "40px",
            paddingRight: "40px",
            paddingTop: "20px",
          }}
        >
          {props.selectedJob.description}
        </p>
        <p>
          <span style={{ fontWeight: "500" }}>
            Salary:{props.selectedJob.salary / 1000}K
          </span>
        </p>
        <div
          style={{
            justifyContent: "center",
            alignSelf: "center",
            flexDirection: "row",
            display: "flex",
          }}
        >
          <button className="btn_Apply">Apply</button>
          <button className="btn_Cancel">Not Intrested</button>
        </div>
      </div>
    </div>
  );
};
const mapStateToProps = (state) => {
  return { selectedJob: state.selectJob };
};

export default connect(mapStateToProps)(JobDetail);
