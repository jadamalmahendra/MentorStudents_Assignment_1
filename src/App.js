import React, { Component } from "react";
import Homepage from "./HomePage";
import Profile from "./components/Profile";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import CompanyList from "./components/CompanyList";
import JobBreifList from "./JobBreifList";

export default class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/jobs" exact component={Homepage}></Route>
          <Route path="/profile" exact component={Profile}></Route>
          <Route path="/companies" exact component={CompanyList}></Route>
          <Route path="/" exact component={Homepage}></Route>
        </Switch>
      </Router>
    );
  }
}
