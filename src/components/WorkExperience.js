import React, { useState } from "react";
import { Button } from "semantic-ui-react";
import WorkExperienceForm from "./WorkExperienceForm";
export default function WorkExperience() {
  const [workdetail, setWorkdetail] = useState([]);

  const removeBtn = (jobTitle) => {
    const removedata = workdetail.filter(
      (job) => workdetail.jobTitle !== jobTitle
    );
    setWorkdetail(removedata);
  };

  const detail = (e) => {
    setWorkdetail([...workdetail, e]);
  };

  return (
    <>
      <div>
        <WorkExperienceForm workdetail={detail} />
      </div>

      {workdetail &&
        workdetail.map((e, index) => (
          <div
            style={{
              marginLeft: "2.5rem",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              width: "70%",
              marginBottom: "2rem",
              marginTop: "1rem",
            }}
            key={index}
          >
            <div className="content">
              <h5
                style={{
                  fontSize: "15px",
                  fontWeight: "800",
                  textTransform: "capitalize",
                  color: "teal",
                }}
              >
                {e.degree}
              </h5>
              <h5>{e.college}</h5>
              <h5>{e.location}</h5>
              <h5>
                {e.monthfrom} {e.yearfrom} to {e.monthto} {e.yearto}
              </h5>
            </div>
            <Button
              variant="contained"
              color="secondary"
              onClick={() => removeBtn(e.jobTitle)}
            >
              Remove
            </Button>
          </div>
        ))}

      <div
        style={{
          display: "flex",
          justifyContent: "space-around",
          position: "fixed",
          bottom: "2rem",
          right: "1rem",
          width: "20%",
        }}
      ></div>
    </>
  );
}
