import React, { useState } from "react";
export default function PersonalInformation() {
  const [userInfo, setState] = useState({
    firstName: "",
    lastName: "",
    email: "",
    mobileNumber: "",
    city: "",
    postalCode: "",
    description: "",
  });
  const saveInfo = (e) => {
    e.preventDefault();
    console.log(
      userInfo.firstName,
      userInfo.lastName,
      userInfo.email,
      userInfo.mobileNumber,
      userInfo.postalCode,
      userInfo.city,
      userInfo.description
    );
  };
  const updateField = (e) => {
    setState({
      ...userInfo,
      [e.target.name]: e.target.value,
    });
  };
  return (
    <form className="ui form  segment " onSubmit={saveInfo}>
      <h3>Personal Information</h3>

      <div className="equal width fields">
        <div className="field">
          <label>First name</label>
          <input
            type="text"
            placeholder="First Name"
            value={userInfo.firstName}
            name="firstName"
            onChange={updateField}
          />
        </div>

        <div className="field">
          <label>Last name</label>
          <input
            type="text"
            placeholder="Last Name"
            name="lastName"
            onChange={updateField}
            value={userInfo.lastName}
          />
        </div>
      </div>
      <div className="equal width fields">
        <div className="field">
          <label>Email</label>
          <input
            type="email"
            name="email"
            placeholder="Example@email.com"
            onChange={updateField}
            value={userInfo.email}
          />
        </div>

        <div className="field">
          <label>Mobile Number</label>
          <input
            type="tel"
            name="mobileNumber"
            placeholder="9876543210"
            onChange={updateField}
            value={userInfo.mobileNumber}
          />
        </div>
      </div>

      <div className="equal width fields">
        <div className="field">
          <label>City</label>
          <input
            type="text"
            name="city"
            placeholder="City"
            onChange={updateField}
            value={userInfo.city}
          />
        </div>

        <div className="field">
          <label>Postal Code</label>
          <input
            type="text"
            name="postalCode"
            placeholder="Postal Code"
            onChange={updateField}
            value={userInfo.postalCode}
          />
        </div>
      </div>
      <div class="field">
        <label>
          Describe your position and accomplisments as a Full Stack Developer
        </label>
        <textarea
          rows="2"
          name="description"
          placeholder="Describe your position and accomplisments as a Full Stack Developer"
          onChange={updateField}
          value={userInfo.description}
        ></textarea>
      </div>
      <button className="ui primary button">Save</button>
    </form>
  );
}
