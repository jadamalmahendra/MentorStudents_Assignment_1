import React, { useState } from "react";
import DropDown from "./Dropdown";

export default function WorkExperienceForm({ workdetail }) {
  const years = [];
  const [hidden, setHidden] = useState(true);
  const [work, setWork] = useState({
    jobTitle: "",
    company: "",
    location: "",
    monthfrom: "",
    monthto: "",
    yearfrom: "",
    yearto: "",
  });

  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  const getyear = () => {
    let currentYear = new Date().getFullYear();
    for (let i = currentYear; i >= 2010; i--) {
      years.push(i);
    }
    return years;
  };

  const yearoption = getyear();

  const onSubmitform = (e) => {
    e.preventDefault();

    workdetail(work);
    setWork({
      jobTitle: "",
      company: "",
      location: "",
      monthfrom: "",
      monthto: "",
      yearfrom: "",
      yearto: "",
    });
  };

  const updateField = (e) => {
    setWork({
      ...work,
      [e.target.name]: e.target.value,
    });
  };
  const fromMonth = (e) => {
    setWork({ ...work, monthfrom: e });
  };
  const toMonth = (e) => {
    setWork({ ...work, monthto: e });
  };
  const fromYear = (e) => {
    setWork({ ...work, yearfrom: e });
  };
  const toYear = (e) => {
    setWork({ ...work, yearto: e });
  };

  return (
    <div style={{ marginTop: "10px" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          textAlign: "center",
        }}
      >
        <h3>Work Experience</h3>
        {hidden ? (
          <button
            className="ui button primary"
            onClick={() => setHidden(!hidden)}
          >
            Add
          </button>
        ) : (
          <button className="ui button red" onClick={() => setHidden(!hidden)}>
            Close
          </button>
        )}
      </div>

      <form
        className="ui form  segment"
        onSubmit={onSubmitform}
        hidden={hidden}
      >
        <div className="field">
          <label>Job Title</label>
          <input
            type="text"
            name="jobTitle"
            value={work.jobTitle}
            onChange={updateField}
          />
        </div>
        <div className="field">
          <label>Company</label>
          <input
            type="text"
            name="company"
            value={work.company}
            onChange={updateField}
          />
        </div>
        <div className="field">
          <label>Location</label>
          <input
            type="text"
            name="location"
            value={work.location}
            onChange={updateField}
          />
        </div>
        <div className="equal width fields">
          <div className="field">
            <label>From</label>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
              }}
            >
              <DropDown
                className="ui dropdown"
                selectname="frommonth"
                options={months}
                onSelected={fromMonth}
              />
              <DropDown
                selectname="fromyear"
                options={yearoption}
                onSelected={fromYear}
              />
            </div>
          </div>

          <div className="field">
            <label>To</label>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
              }}
            >
              <DropDown
                label="To"
                selectname="tomonth"
                options={months}
                onSelected={toMonth}
              />
              <DropDown
                label="To"
                selectname="toyear"
                options={yearoption}
                onSelected={toYear}
              />
            </div>
          </div>
        </div>
        <button className="ui button primary" type="submit">
          Save
        </button>
      </form>
    </div>
  );
}
