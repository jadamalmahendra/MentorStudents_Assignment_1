import React, { useState } from "react";
import { Button } from "semantic-ui-react";
import SkillinfoForm from "./SkillInfoForm";
export default function Skils() {
  const [skillInfo, setSkillInfo] = useState([]);

  const details = (e) => {
    setSkillInfo([...skillInfo, e]);
  };

  const removeBt = (skillname) => {
    const removedata = skillInfo.filter((skill) => skill.skill !== skillname);
    console.log(removedata);
    setSkillInfo(removedata);
  };

  return (
    <>
      <div>
        <SkillinfoForm skilldetails={details} />
      </div>
      {skillInfo &&
        skillInfo.map((e, index) => (
          <div
            style={{
              marginLeft: "2.5rem",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              width: "70%",
              marginBottom: "2rem",
              marginTop: "1rem",
            }}
            key={index}
          >
            <div>
              <h5
                style={{
                  fontSize: "15px",
                  fontWeight: "800",
                  textTransform: "capitalize",
                  color: "teal",
                }}
              >
                {e.skill}
              </h5>
              <h5>{e.rating}/10</h5>
            </div>
            <Button onClick={() => removeBt(e.skill)}>Remove</Button>
          </div>
        ))}

      <div
        style={{
          display: "flex",
          justifyContent: "space-around",
          position: "fixed",
          bottom: "2rem",
          right: "1rem",
          width: "20%",
        }}
      ></div>
    </>
  );
}
