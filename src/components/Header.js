import React from "react";
import logo from "../assets/images/LogoBlue.png";
import { Link } from "react-router-dom";
import { Button } from "semantic-ui-react";
import GoogleAuth from "./GoogleAuth";

export default function Header() {
  return (
    <div
      className="ui"
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        paddingLeft: "20px",
        paddingRight: "20px",
        paddingTop: "10px",
        paddingBottom: "10px",
        position: "sticky",
        backgroundColor: "white",
        top: 0,
        zIndex: "20",
      }}
    >
      <Link to="/">
        <img
          src={logo}
          style={{
            width: "200px",
            backgroundColor: "white",
            borderRadius: "5px",
          }}
          alt="logo"
        />
      </Link>
      <div
        className="ui four item  "
        style={{ marginRight: "20px", display: "flex" }}
      >
        <Link to="/jobs" style={{ marginRight: "10px" }}>
          <Button className="item">View Jobs </Button>
        </Link>
        <Link to="/profile">
          <Button className="item" style={{ marginRight: "10px" }}>
            see your profile
          </Button>
        </Link>
        <Link to="/companies" style={{ marginRight: "10px" }}>
          <Button className="item">companies</Button>
        </Link>
        <div className="item" style={{ marginRight: "10px" }}>
          <GoogleAuth />
        </div>
      </div>
    </div>
  );
}
