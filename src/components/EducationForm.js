import React, { useState } from "react";
import DropDown from "./Dropdown";

export default function EducationForm({ educationdetails }) {
  const years = [];
  const [education, setEducation] = useState({
    degree: "",
    college: "",
    location: "",
    monthfrom: "",
    monthto: "",
    yearfrom: "",
    yearto: "",
  });

  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  const getyear = () => {
    let currentYear = new Date().getFullYear();
    for (let i = currentYear; i >= 2010; i--) {
      years.push(i);
    }
    return years;
  };

  const yearoption = getyear();
  const [hidden, setHidden] = useState(true);

  const formSubmit = (e) => {
    e.preventDefault();
    educationdetails(education);
    setEducation({
      degree: "",
      college: "",
      location: "",
      monthfrom: "",
      monthto: "",
      yearfrom: "",
      yearto: "",
    });
  };

  const updateField = (e) => {
    setEducation({
      ...education,
      [e.target.name]: e.target.value,
    });
  };
  const fromMonth = (e) => {
    setEducation({ ...education, monthfrom: e });
  };
  const toMonth = (e) => {
    setEducation({ ...education, monthto: e });
  };
  const fromYear = (e) => {
    setEducation({ ...education, yearfrom: e });
  };
  const toYear = (e) => {
    setEducation({ ...education, yearto: e });
  };

  return (
    <div style={{ marginTop: "10px" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          textAlign: "center",
        }}
      >
        <h3>Education</h3>
        {hidden ? (
          <button
            className="ui button primary"
            onClick={() => setHidden(!hidden)}
          >
            Add
          </button>
        ) : (
          <button className="ui button red" onClick={() => setHidden(!hidden)}>
            Close
          </button>
        )}
      </div>

      <form className="ui form  segment" onSubmit={formSubmit} hidden={hidden}>
        <div className="field">
          <label>Degree</label>
          <input
            type="text"
            name="degree"
            value={education.degree}
            onChange={updateField}
          />
        </div>
        <div className="field">
          <label>College</label>
          <input
            type="text"
            name="college"
            value={education.college}
            onChange={updateField}
          />
        </div>
        <div className="field">
          <label>Location</label>
          <input
            type="text"
            name="location"
            value={education.location}
            onChange={updateField}
          />
        </div>
        <div className="equal width fields">
          <div className="field">
            <label>From</label>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
              }}
            >
              <DropDown
                className="ui dropdown"
                selectname="frommonth"
                options={months}
                onSelected={fromMonth}
              />
              <DropDown
                selectname="fromyear"
                options={yearoption}
                onSelected={fromYear}
              />
            </div>
          </div>

          <div className="field">
            <label>To</label>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
              }}
            >
              <DropDown
                label="To"
                selectname="tomonth"
                options={months}
                onSelected={toMonth}
              />
              <DropDown
                label="To"
                selectname="toyear"
                options={yearoption}
                onSelected={toYear}
              />
            </div>
          </div>
        </div>
        <button className="ui button primary" type="submit">
          Save
        </button>
      </form>
    </div>
  );
}
