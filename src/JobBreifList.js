import React, { Component } from "react";
import JobDetail from "./JobDetail";
import { connect } from "react-redux";
import { selectJob } from "./actions";
class JobBreifList extends Component {
  renderJobList() {
    return this.props.jobs.map((job) => {
      const { name, location, salary, description, logo } = job;
      const { city, country } = location;
      return (
        <div
          className="jobs"
          key={job}
          onClick={() => this.props.selectJob(job)}
        >
          <img src={logo} alt="logo" />
          <div className="jobDetails">
            <p style={{ fontWeight: "500", fontSize: "20px" }}>{name}</p>
            <p style={{ fontSize: "15px" }}>
              {city}, {country}
            </p>
            <p
              style={{
                marginTop: "5px",
                marginBottom: "5px",
                fontSize: "13px",
                lineHeight: "1em",
                maxHeight: "3em",
                overflow: "hidden",
              }}
            >
              {description}
            </p>
            <p>
              <span style={{ fontWeight: "500" }}>Salary:</span> {salary / 1000}
              K
            </p>
            <div className="btns">
              <button className="btn_Apply">Apply</button>
              <button className="btn_Cancel">Not Intrested</button>
            </div>
          </div>
        </div>
      );
    });
  }
  render() {
    return (
      <div style={{ display: "flex", flexDirection: "row" }}>
        <div style={{ display: "flex", flexDirection: "column" }}>
          {this.renderJobList()}
        </div>
        <JobDetail />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  console.log(state);
  return { job: state.jobs };
};

export default connect(mapStateToProps, { selectJob })(JobBreifList);
